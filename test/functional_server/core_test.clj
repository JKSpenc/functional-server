(ns functional-server.core-test
  (:require [clojure.test :refer :all]
            [functional-server.core :refer :all])
  (:import (org.bson.types ObjectId)))

(deftest get-neighbours-test
  (let [grid [
              [{} {} {} {} {}]
              [{} {} {} {} {}]
              [{} {} {} {} {}]
              [{} {} {} {} {}]
              [{} {} {} {} {}]
              ]]
    (testing "We get the correct neighbours for a cell"
      (is (= (get-neighbours 0 0 grid) [[:down [1 0]]
                                        [:right [0 1]]]))
      (is (= (get-neighbours 4 4 grid) [[:up [3 4]]
                                        [:left [4 3]]]))
      (is (= (get-neighbours 2 2 grid) [[:up [1 2]]
                                        [:left [2 1]]
                                        [:down [3 2]]
                                        [:right [2 3]]
                                        ]))
      )
    (testing "Boundary conditions"
      (is (= (get-neighbours -1 9 grid) []))
      )
    )
  )


(deftest maze-generation
  (testing "We can generate a square maze"
    (let [maze (create-maze 5 5)]
      (is (not (nil? maze)))
      (is (= 5 (count maze)))
      (is (= 5 (count (first maze))))
      )
    )
  (testing "We can generate a rectangle maze"
    (let [maze (create-maze 10 5)]
      (is (not (nil? maze)))
      (is (= 10 (count maze)))
      (is (= 5 (count (first maze))))
      )
    (let [maze (create-maze 5 10)]
      (is (not (nil? maze)))
      (is (= 5 (count maze)))
      (is (= 10 (count (first maze))))
      )
    )
  )

(deftest maze-storage
  (testing "We can store a maze with a name"
    (let [maze (create-maze 5 5)]
      (is (= (type ((store-maze maze "TESTMAZE") :id)) ObjectId))
      )
    )

  (testing "We can list the mazes and it includes the newly created one"
    (let [mazes (filter #(= (% :name) "TESTMAZE") (list-mazes))]
      (is (= (count mazes) 1))
      (is (= (get (first mazes) :height) 5))
      (is (= (get (first mazes) :width) 5))
      (is (= (get (first mazes) :name) "TESTMAZE"))
      )
    )
  (testing "We can delete the created maze"
    (let [mazes (filter #(= (% :name) "TESTMAZE") (list-mazes))]
      (is (= (count mazes) 1))
      (is (not (nil? (get (first mazes) :_id))))
      (is (= (delete-maze (.toString (get (first mazes) :_id))) {:count 1}))
      (is (= (count (filter #(= (% :name) "TESTMAZE") (list-mazes))) 0))
      )
    )
  )