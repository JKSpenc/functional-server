(defproject functional_server "0.1.0-SNAPSHOT"
  :description "A server to generate mazes"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [com.novemberain/monger "3.1.0"]
                 [ring "1.8.0"]
                 [ring/ring-defaults "0.3.2"]
                 [stencil "0.5.0"]
                 [compojure "1.6.1"]
                 [cheshire "5.10.0"]]
  :ring {:handler functional-server.core/-main}
  :main functional-server.core
  :repl-options {:init-ns functional-server.core})
