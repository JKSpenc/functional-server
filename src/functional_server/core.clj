(ns functional-server.core
  (:require
    [stencil.core :refer [render-string]]
    [ring.adapter.jetty :as jetty]
    [ring.middleware.params :refer [wrap-params]]
    [ring.middleware.defaults :refer :all]
    [compojure.core :refer :all]
    [compojure.route :as route]
    [compojure.handler :refer [site]]
    [monger.core :as mg]
    [monger.collection :as mc]
    [cheshire.core :refer :all]
    [cheshire.generate :refer [add-encoder encode-str remove-encoder]])
  (:import (org.bson.types ObjectId)
           (com.fasterxml.jackson.core.json WriterBasedJsonGenerator)))

(add-encoder ObjectId
             (fn [^ObjectId id ^WriterBasedJsonGenerator generator]
               (.writeString generator (.toString id))))

(defn gen-grid [height width] (loop [i 0 grid []]
                                (cond
                                  (= i height) grid
                                  :else (recur (inc i) (conj grid (vec (repeat width {}))))
                                  )
                                ))

(defn get-maze
  [^String id]
  (let [
        conn (mg/connect)
        db   (mg/get-db conn "maze-db")
        ]
    (mc/find-one-as-map db "mazes" { :_id (ObjectId. id) })
    )
  )

(defn list-mazes
  []
  (let [
        conn (mg/connect)
        db   (mg/get-db conn "maze-db")
        ]
    (mc/find-maps db "mazes")
    )
  )

(defn store-maze
  [maze ^String name]
  (let [
        conn (mg/connect)
        db   (mg/get-db conn "maze-db")
        height (count maze)
        width (count (first maze))
        id (ObjectId.)
        ]
    (mc/insert db "mazes" {:_id id :name name :maze maze :height height :width width})
    {:id id}
    )
  )

(defn delete-maze
  [^String id]
  (let [
        conn (mg/connect)
        db   (mg/get-db conn "maze-db")
        ]
    {:count (.getN (mc/remove-by-id db "mazes" (ObjectId. id)))}
    )
  )

; Used for debugging purposes
(defn print-maze [maze]
  (let [h (count maze)
        w (count (maze 0))
        grid-top (loop [x 0 top ""]
                   (cond
                     (= x w) top
                     :else (recur (inc x) (str top "---+"))
                     )
                   )]
    (loop [i 0 j 0 top "|" bottom "+" grid-str (str "+" grid-top "\n")]
      (cond
        (= i h) grid-str
        :else (if (= j w)
                (do
                  (recur (inc i) 0 "|" "+" (str grid-str top "\n" bottom "\n"))
                  )
                (let [cell (get-in maze [i j])
                      body "   "
                      corner "+"
                      east (if (true? (cell :right))
                             " "
                             "|")
                      south (if (true? (cell :down))
                              "   "
                              "---")]

                  (recur i (inc j) (str top body east) (str bottom south corner) grid-str)
                  )
                )
        ))
    )
  )

(defn get-opposite
  [dir]
  (cond
    (= dir :up) :down
    (= dir :down) :up
    (= dir :left) :right
    (= dir :right) :left
    ))

(defn get-neighbours
  [x y grid]
  (filter #(get-in grid (last %)) [[:up [(- x 1) y]]
                                   [:left [x (- y 1)]]
                                   [:down [(+ x 1) y]]
                                   [:right [x (+ y 1)]]
                                   ]
          )
  )

(def get-neighbours-memo (memoize get-neighbours))

(defn find-unvisited
  [[x y] grid visited]
  (let [neighbours (get-neighbours-memo x y grid)]
    (filter #(nil? (get visited (last %))) neighbours)
    )
  )

(def find-unvisited-memo (memoize find-unvisited))

(defn remove-wall
  [current-cell neighbour grid]
  (-> grid
      (update-in current-cell conj {(first neighbour) true})
      (update-in (last neighbour) conj {(get-opposite (first neighbour)) true}))
  )

(def remove-wall-memo (memoize remove-wall))

(defn create-maze
  [^Number h ^Number w]
  (loop [grid (gen-grid h w)
         visited {[0 0] true}
         stack [[0 0]]
         ]
    (if (empty? stack)
      grid
      (let [unvisited (find-unvisited-memo (first stack) grid visited)]
        (if (empty? unvisited)
          (recur grid visited (rest stack))
          (let [chosen (rand-nth unvisited)]
            (recur (remove-wall-memo (first stack) chosen grid)
                   (conj visited {(last chosen) true})
                   (cons (last chosen) stack))
            )
          )
        )
      )
    )
  )

(defroutes handler
           (GET "/mazes" [] {:status 200
                             :headers {"Content-Type" "application/json"}
                             :body (generate-string (list-mazes))})
           (GET "/maze/:id" [id] {:status 200
                                  :headers {"Content-Type" "application/json"}
                                  :body (generate-string (get-maze id))})
           (POST "/gen-maze" [height width name] {:status 200
                                             :headers {"Content-Type" "application/json"}
                                             :body (generate-string (store-maze (create-maze
                                                                 (Integer/parseInt height)
                                                                 (Integer/parseInt width)) name))})
           (DELETE "/delete/:id" [id] {:status 200
                                       :headers {"Content-Type" "application/json"}
                                       :body (generate-string (delete-maze id))})
           )

(defn -main []
  (jetty/run-jetty (wrap-params handler (assoc site-defaults :security false)) {:port 3000}))